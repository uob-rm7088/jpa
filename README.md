To run:

1. Checkout this repository
2. Change into the directory you checked out
3. On the command line, execute `mvn spring-boot:run`
4. [Click here](localhost:8080/buildings)