package uk.ac.bris.adt.orm.jpa.model;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@NamedEntityGraph(name = "Building.detail", attributeNodes = @NamedAttributeNode("rooms"))
public class Building {
  @Id
  @GeneratedValue
  private Integer id;

  private String name;

  @OneToMany(mappedBy = "building", cascade = CascadeType.REMOVE)
  private List<Room> rooms;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Room> getRooms() {
    return rooms;
  }

  public void setRooms(List<Room> rooms) {
    this.rooms = rooms;
  }

  @Override
  public String toString() {
    return name;
  }
}
