package uk.ac.bris.adt.orm.jpa.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import uk.ac.bris.adt.orm.jpa.model.Building;
import uk.ac.bris.adt.orm.jpa.model.Room;
import uk.ac.bris.adt.orm.jpa.repository.RoomRepository;

@Controller
@RequestMapping("/rooms")
public class RoomController {
  private final RoomRepository roomRepository;

  public RoomController(RoomRepository roomRepository) {
    this.roomRepository = roomRepository;
  }

  @InitBinder
  public void initBinder(WebDataBinder binder) {
    binder.setAllowedFields("name");
  }

  @GetMapping("/add")
  public String add(@RequestParam("building.id") Building building, Model model) {
    Room room = new Room();
    room.setBuilding(building);
    model.addAttribute(room);
    return "rooms/add";
  }

  @PostMapping
  public String create(@RequestParam("building.id") Building building, @ModelAttribute Room roomForm,
                       RedirectAttributes redirectAttributes) {
    roomForm.setBuilding(building);
    Room room = roomRepository.save(roomForm);
    redirectAttributes.addFlashAttribute("message", "Added room " + room + " to building " + room.getBuilding());
    return "redirect:/buildings/" + room.getBuilding().getId();
  }

  @GetMapping("/{id}/edit")
  public String edit(@PathVariable("id") Room room, Model model) {
    model.addAttribute(room);
    return "rooms/edit";
  }

  @PostMapping("/{id}")
  public String update(@PathVariable("id") Room room, @ModelAttribute Room roomForm,
                       RedirectAttributes redirectAttributes) {
    room.setName(roomForm.getName());
    roomRepository.save(room);
    redirectAttributes.addFlashAttribute("message", "Updated room " + room + " in building " + room.getBuilding());
    return "redirect:/buildings/" + room.getBuilding().getId();
  }

  @DeleteMapping("/{id}")
  public String delete(@PathVariable("id") Room room, RedirectAttributes redirectAttributes) {
    roomRepository.delete(room);
    redirectAttributes.addFlashAttribute("message", "Removed room " + room + " from building " + room.getBuilding());
    return "redirect:/buildings/" + room.getBuilding().getId();
  }
}