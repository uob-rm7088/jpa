package uk.ac.bris.adt.orm.jpa.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import uk.ac.bris.adt.orm.jpa.model.Building;
import uk.ac.bris.adt.orm.jpa.repository.BuildingRepository;

@Controller
@RequestMapping("/buildings")
public class BuildingController {
  private final BuildingRepository buildingRepository;

  public BuildingController(BuildingRepository buildingRepository) {
    this.buildingRepository = buildingRepository;
  }

  @InitBinder
  public void initBinder(WebDataBinder binder) {
    binder.setAllowedFields("name");
  }

  @GetMapping
  public String index(Model model) {
    Iterable<Building> buildingList = buildingRepository.findAll();
    model.addAttribute(buildingList);
    return "buildings/index";
  }

  @GetMapping("/{id}")
  public String show(@PathVariable("id") Building building, Model model) {
    model.addAttribute(building);
    return "buildings/show";
  }

  @GetMapping("/add")
  public String add(Model model) {
    Building building = new Building();
    model.addAttribute(building);
    return "buildings/add";
  }

  @PostMapping
  public String create(@ModelAttribute Building buildingForm, RedirectAttributes redirectAttributes) {
    Building building = buildingRepository.save(buildingForm);
    redirectAttributes.addFlashAttribute("message", "Created building " + building.getName());
    return "redirect:/buildings/" + building.getId();
  }

  @GetMapping("/{id}/edit")
  public String edit(@PathVariable("id") Building building, Model model) {
    model.addAttribute(building);
    return "buildings/edit";
  }

  @PostMapping("/{id}")
  public String update(@PathVariable("id") Building building, @ModelAttribute Building buildingForm,
                       RedirectAttributes redirectAttributes) {
    building.setName(buildingForm.getName());
    buildingRepository.save(building);
    redirectAttributes.addFlashAttribute("message", "Updated building " + building.getName());
    return "redirect:/buildings/" + building.getId();
  }

  @DeleteMapping("/{id}")
  public String delete(@PathVariable("id") Building building, RedirectAttributes redirectAttributes) {
    buildingRepository.delete(building);
    redirectAttributes.addFlashAttribute("message", "Deleted building " + building.getName());
    return "redirect:/buildings";
  }
}