package uk.ac.bris.adt.orm.jpa.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;
import uk.ac.bris.adt.orm.jpa.model.Building;

import javax.inject.Named;

@Named
public interface BuildingRepository extends CrudRepository<Building, Integer> {
  @EntityGraph(value = "Building.detail", type = EntityGraph.EntityGraphType.LOAD)
  Building findOne(Integer id);
}
