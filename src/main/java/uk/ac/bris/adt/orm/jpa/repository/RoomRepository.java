package uk.ac.bris.adt.orm.jpa.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;
import uk.ac.bris.adt.orm.jpa.model.Building;
import uk.ac.bris.adt.orm.jpa.model.Room;

import javax.inject.Named;

@Named
public interface RoomRepository extends CrudRepository<Room, Integer> {
}
