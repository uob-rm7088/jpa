<jsp:include page="../shared/header.jsp"/>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--@elvariable id="room" type="uk.ac.bris.adt.orm.jpa.model.Room"--%>

<h2>Edit room - ${room.name}</h2>

<form:form method="post" servletRelativeAction="/rooms/${room.id}" modelAttribute="room">
  <div class="form-group">
    <form:label path="name" cssClass="control-label">Name</form:label>
    <form:input path="name" cssClass="form-control"/>
  </div>

  <form:hidden path="building.id"/>

  <button type="submit" class="btn btn-success">Update</button>
  <a href="<c:url value="/buildings/${room.building.id}"/>" class="btn btn-default">Cancel</a>
</form:form>

<jsp:include page="../shared/footer.jsp"/>