<jsp:include page="../shared/header.jsp"/>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--@elvariable id="building" type="uk.ac.bris.adt.orm.jpa.model.Building"--%>

<h2>Edit building - ${building.name}</h2>

<form:form method="post" servletRelativeAction="/buildings/${building.id}" modelAttribute="building">
  <div class="form-group">
    <form:label path="name" cssClass="control-label">Name</form:label>
    <form:input path="name" cssClass="form-control"/>
  </div>
  <button type="submit" class="btn btn-success">Update</button>
  <a href="<c:url value="/buildings/${building.id}"/>" class="btn btn-default">Cancel</a>
</form:form>

<jsp:include page="../shared/footer.jsp"/>