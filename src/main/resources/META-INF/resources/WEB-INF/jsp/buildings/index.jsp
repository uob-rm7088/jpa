<jsp:include page="../shared/header.jsp"/>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--@elvariable id="buildingList" type="java.util.List<uk.ac.bris.adt.orm.jpa.model.Building>"--%>

<h2>Buildings</h2>

<table class="table">
  <thead>
  <tr>
    <th>Name</th>
    <th>
      <a href="<c:url value="/buildings/add"/>" class="btn btn-sm btn-success">Add</a>
    </th>
  </tr>
  </thead>
  <tbody>
  <c:forEach var="building" items="${buildingList}">
    <tr>
      <td>${building.name}</td>
      <td>
        <a href="<c:url value="/buildings/${building.id}"/>" class="btn btn-sm btn-default">View</a>
        <a href="<c:url value="/buildings/${building.id}/edit"/>" class="btn btn-sm btn-primary">Edit</a>
        <form:form method="delete" servletRelativeAction="/buildings/${building.id}" cssClass="delete">
          <button type="submit" class="btn btn-sm btn-danger">Delete</button>
        </form:form>
      </td>
    </tr>
  </c:forEach>
  </tbody>
</table>

<jsp:include page="../shared/footer.jsp"/>