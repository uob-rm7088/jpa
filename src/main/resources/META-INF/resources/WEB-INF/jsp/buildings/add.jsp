<jsp:include page="../shared/header.jsp"/>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h2>Add building</h2>

<form:form method="post" servletRelativeAction="/buildings" modelAttribute="building">
  <div class="form-group">
    <form:label path="name" cssClass="control-label">Name</form:label>
    <form:input path="name" cssClass="form-control"/>
  </div>

  <button type="submit" class="btn btn-success">Create</button>
  <a href="<c:url value="/buildings"/>" class="btn btn-default">Cancel</a>
</form:form>

<jsp:include page="../shared/footer.jsp"/>