<jsp:include page="../shared/header.jsp"/>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--@elvariable id="building" type="uk.ac.bris.adt.orm.jpa.model.Building"--%>

<h2>Building - ${building.name}</h2>

<a href="<c:url value="/buildings"/>" class="btn btn-sm btn-default">View All</a>
<a href="<c:url value="/buildings/${building.id}/edit"/>" class="btn btn-sm btn-primary">Edit</a>
<form:form method="delete" servletRelativeAction="/buildings/${building.id}" cssClass="delete">
  <button type="submit" class="btn btn-sm btn-danger">Delete</button>
</form:form>

<h3>Rooms</h3>
<table class="table">
  <thead>
  <tr>
    <th>Name</th>
    <th>
      <a href="<c:url value="/rooms/add?building.id=${building.id}"/>" class="btn btn-sm btn-success">Add</a>
    </th>
  </tr>
  </thead>
  <tbody>
  <c:forEach var="room" items="${building.rooms}">
    <tr>
      <td>${room.name}</td>
      <td>
        <a href="<c:url value="/rooms/${room.id}/edit"/>" class="btn btn-sm btn-primary">Edit</a>
        <form:form method="delete" servletRelativeAction="/rooms/${room.id}" cssClass="delete">
          <button type="submit" class="btn btn-sm btn-danger">Delete</button>
        </form:form>
       </td>
    </tr>
  </c:forEach>
  </tbody>
</table>

<jsp:include page="../shared/footer.jsp"/>